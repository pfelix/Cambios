﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cambios.Modelos
{
    //Modelo que suporta os dados que a API devolve
    //A API devolve em JSON é necessário instalar nos NuGets a opção Newtonsoft.Json


    public class Rate
    {
        public int RateId { get; set; }
        public string Code { get; set; }
        public double TaxRate { get; set; }
        public string Name { get; set; }

    }
}
