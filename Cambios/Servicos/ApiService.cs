﻿using Cambios.Modelos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Cambios.Servicos
{
    public class ApiService
    {
        //Task parefa Response
        public async Task<Response> GetRates(string urlBase, string controller)
        {
            try
            {
                var client = new HttpClient();
                //Endereço da API
                client.BaseAddress = new Uri(urlBase);

                //Contrulador
                //Metemos await para que a aplicação continue a correr
                //E assim temos que meter o metodo com async
                //O resultado guardamos numa variável
                var response = await client.GetAsync(controller);

                //Guarda o resultado da resposta do controlado
                var result = await response.Content.ReadAsStringAsync();

                //Verifica se ligou-se à API
                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result
                    };
                }

                //Converte os dados de Json para uma lista de objeto Rate
                var rates = JsonConvert.DeserializeObject<List<Rate>>(result);

                return new Response
                {
                    IsSuccess = true,
                    Result = rates
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
